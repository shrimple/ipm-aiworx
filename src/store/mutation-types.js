export const TOGGLE_DEVICE = 'TOGGLE_DEVICE';

export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';

export const EXPAND_MENU = 'EXPAND_MENU';

export const LOGIN = 'LOGIN';

export const LOGOUT = 'LOGOUT';
