import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import alert from './modules/alert';
import user from './modules/user';
import app from './modules/app';
import routes from './modules/routes';
import setupFeathersData from './modules/feathersData'

Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['app', 'user', 'data']
    })],
    strict: false,  // process.env.NODE_ENV !== 'production',

    modules: {
        user,
        app,
        alert,
        routes,
    },
    // getters: {
    // 	services: state => {
    // 		return state.data && state.data.services || null
    // 	}
    // }
});


if (store.state.user.firstName) {
    setupFeathersData(false, store)
}

export default store;