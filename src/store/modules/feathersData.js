import Feathers from "../../modules/feathers-client";
import Vue from "vue";

const services = ['project', 'project-form', 'todo', 'image'];

export const refreshServices = (store) => {
    services.forEach(s => {
        const service = Feathers.service(s);
        const serviceName = s.replace('-', '_');
        service.find().then(r => {
            store.commit('find', {data: r.data, serviceName})
        });
        service.off('created');
        service.on('created', data => {
            service.get(data.id).then(newData => {
                store.commit('create', {newData, serviceName})
            });
        });
        service.off('removed');
        service.on('removed', data => {
            store.commit('remove', {data, serviceName})
        });
        service.off('updated');
        service.on('updated', data => {
            service.get(data.id).then(newData => {
                store.commit('update', {newData, serviceName})
            });
        });
        service.off('patched');
        service.on('patched', data => {
            service.get(data.id).then(newData => {
                store.commit('update', {newData, serviceName})
            })
        });
    });
};

export default async (preserve, store) => {
    const module = {
        state: {
            services: {
                project: [],
                project_form: [],
                todo: [],
                image: [],
            }
        },
        mutations: {
            find(state, params) {
                state.services[params.serviceName] = params.data;
            },
            create(state, params) {

                let data = getDataFromParams(params);

                if (data == {}) {
                    console.log("[feathersData/mutations:create] -> Got no data?");
                    return;
                }

                state.services[params.serviceName].push(data);

                associateDataToParents(params.serviceName, state, data, determineParents(data));
                console.log("[feathersData/mutations:create] -> Data of id '" + data.id + "', associated to parents.");
            },

            remove(state, params) {
                const array = state.services[params.serviceName];
                let data = getDataFromParams(params);
                //console.log(params.serviceName);

                array.forEach((r, i) => {
                    if (r.id === params.data.id) {
                        array.splice(i, 1);
                    }
                });

                let id = data.id;
                removeAssociations(state, data, params.serviceName);
                console.log("[feathersData/mutations:remove] -> Removed associated data of id '" + id + "'.");
            },

            update(state, params) {

                console.log(params);

                let data = getDataFromParams(params);

                if (data == {}) {
                    console.log("[feathersData/mutations:update] -> Got no data?");
                    return;
                }

                state.services[params.serviceName].forEach((r, i) => {
                    if (r.id === data.id) {
                        Object.keys(data).map((key, index) => {
                            if (r[key] !== data[key]) {
                                Vue.set(r, key, data[key]);
                            }
                        })
                    }
                });

                associateDataToParents(params.serviceName, state, data, determineParents(data));
                console.log("[feathersData/mutations:update] -> updated data of id '" + data.id + "'.");
            }
        },

        actions: {
            populate({dispatch, commit, rootState}) {
                registerServices(commit, rootState);
            },
            refresh({dispatch, commit, rootState}) {
                refresh(commit, rootState)
            }
        },

        getters: {
            services: state => {
                return state && state.services || [];
            },
            projects: state => {
                return state.services && state.services.project || [];
            },
            todos: state => {
                return state.services && state.services.todo || [];
            },
            images: state => {
                return state.services && state.services.image || [];
            }
        }
    };

    /** getDataFromParams
     * This function will extract the data.
     * Required function because params data from mutations is not always named consistently (data key changes names).
     * @param obj
     * OBJECT - object containing the data under a variable key.
     */
    function getDataFromParams(obj) {
        let data = {};

        if (!obj)
            return data;

        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (key.includes('data') || key.includes('Data')) {
                    data = obj[key];
                }
            }
        }

        return data;

    }

    /** associateDataToParents
     *  Associates some data to given parents in the services.
     *  Will extract matching child data in parents. If it finds none, mutateParentDataService will handle accordingly.
     * @param childService
     * STRING - the service to which data parameter belongs to.
     * @param state
     * OBJECT - for accessing services.
     * @param data
     * OBJECT - data that has been created or modified.
     * @param parents
     * ARRAY<STRING> - parents that are associated to the data parameter.
     */
    function associateDataToParents(childService, state, data, parents) {
        //parents is a list of service names (Strings)!!!!!!!!!
        for (let parent of parents) {
            //format association key
            if (!state.services[parent]) {
                console.log("-[feathersData/assData2Parents] -> Warning! Tried associating data to a service that does not exist. parentService = " + parent);
                break;
            }

            let associatedIdKey = snakeToCamel(parent) + 'Id';

            //get parent key that associates the child
            let associatedParentId = (data[associatedIdKey]) ? data[associatedIdKey] : -1;

            //catch undefined behaviour
            if (associatedParentId < 0) {
                console.log("-[feathersData/assData2Parents] -> Error while getting associated parent id. Function returning.");
                return;
            }

            //get parent data from services
            let parentServiceData = state.services[parent].find((p) => p.id === associatedParentId);

            //set children values in parents
            mutateParentServiceData(parentServiceData, parent, childService, data, associatedIdKey)
        }
    }


    /**mutateParentServiceData
     * This function will mutate the serviceData with the parameter data.
     * Used directly by associateDataToParents.
     * @param serviceData
     * OBJECT - associated data in parent service
     * @param parent
     * STRING - parent service
     * @param childService
     * STRING - service that owns data param.
     * @param data
     * OBJECT - data that has been created or patched
     */
    function mutateParentServiceData(serviceData, parent, childService, data) {
        let foundKeyInParent = false;

        for (let key in serviceData) {
            if (serviceData.hasOwnProperty(key)) {

                //if key is an array
                if (key.includes(childService) && key[key.length - 1] === 's') {
                    let associatedParentData = serviceData[key].filter(function (p) {
                        return p.id === data.id;
                    });

                    //if it DOESNT find an occurence of the newdata in parent
                    if (associatedParentData.length < 1) {
                        serviceData[key].push(data);

                    } else if (associatedParentData.length > 0 && associatedParentData.length < 2) {
                        associatedParentData = data;

                    } else {
                        return;
                    }

                    foundKeyInParent = true;

                } else if (key.includes(childService)) {
                    serviceData[key] = data;
                    console.log(serviceData[key]);
                    foundKeyInParent = true;
                }
            }
        }

        //this will create associations if they are not found (takes into count if hasMany or hasOne)
        if (!foundKeyInParent) {
            let childKey = (getAssociationType(childService) === 'mult') ? (childService + 's') : (childService);

            if (getAssociationType(childService) === 'single') {
                Vue.set(serviceData, childKey, data);
                console.log(serviceData[childKey]);
            } else {
                let arr = [];
                arr.push(data);
                Vue.set(serviceData, childKey, arr);
            }
        }
    }

    //will remove associated children and will unassociate from parents
    function removeAssociations(state, data, childService) {
        for (let s of services) {
            let service = kebabToSnake(s);
            let dataToDelete = getAssociatedDataInService(state, data, childService, service);


            if (dataToDelete != null) {
                if (dataToDelete.indexesInParent.length > 0) {

                    for (let index of dataToDelete.indexesInParent) {
                        dataToDelete.childDataInParent.splice(index, 1);
                    }
                } else {
                    dataToDelete.childDataInParent = null;
                }

                for (let cData of dataToDelete.dataInChild) {
                    Feathers.service(s).remove(cData.id);
                }

            } else {
                console.warn("-[remAss] -> No parent data found");
            }
        }
    }

    //function used in remove mutation, but could be used for other purposes.
    //will get all data in all services matching the id of parameter 'data'.
    function getAssociatedDataInService(state, data, childService, service) {
        let associatedChildData = [];
        let associatedDataInParent = null;
        let associatedParentIndexes = [];
        let associatedId = data.id;
        let assType = getAssociationType(service);
        let serviceArr = state.services[service];
        let foundData = false;

        if (serviceArr === null)
            return [];

        // console.log("--[getAssDInService] -> assId: " + associatedId + ", childService: " + childService + ", service: " + service + ".");

        for (let serviceData of serviceArr) {

            for (let key in serviceData) {

                if (serviceData.hasOwnProperty(key)) {
                    let idKey = snakeToCamel(childService) + 'Id';

                    if (key.includes(idKey)) {
                        if (serviceData[key] === associatedId) {
                            associatedChildData.push(serviceData);
                            foundData = true;
                        }

                        break;

                    } else if (key.includes(childService)) {
                        //if its an array
                        if (key[key.length - 1] === "s") {
                            associatedDataInParent = serviceData[key];

                            var index = serviceData[key].map(function (d) {
                                return d.id;
                            }).indexOf(data.id);

                            if (index > -1) {
                                associatedParentIndexes.push(index);
                                foundData = true;
                            }

                            break;

                        } else {
                            associatedDataInParent = serviceData[key];
                            foundData = true;
                        }

                        if (assType === "single") {
                            if (foundData) {
                                break;
                            }
                        }

                        break;
                    }
                }
            }

            if (assType === "single") {
                if (foundData) {
                    break;
                }
            }
        }

        if (associatedChildData === [] && associatedDataInParent === null && associatedParentIndexes === []) {
            return null;
        }

        return {
            dataInChild: associatedChildData,
            childDataInParent: associatedDataInParent,
            indexesInParent: associatedParentIndexes,
        };
    }

    function dataContainsKeyStrict(data, searchKey) {
        for (let key of data) {
            if (key === searchKey) {
                return true;
            }
        }

        return false;
    }

    function dataContainsKey(data, searchKey) {
        for (let key in data) {
            if (key.includes(searchKey)) {
                return true;
            }
        }

        return false;

    }

    //hardcoded association types :(
    // *********VERY IMPORTANT IF YOU ADD MORE SERVICES DOWN THE LINE TO DEFINE THEIR ASSOCIATION TYPE**********
    function getAssociationType(serviceName) {
        switch (serviceName) {
            case 'project':
                return 'single';
            case 'project_form':
                return 'mult';
            case 'todo':
                return 'mult';
            case 'image':
                return 'single';
            default:
                console.warn("[GetAssType] -> Found no service with name '" + serviceName + "' ");
                return null;
        }
    }

    //will iterate through an object's keys and find its parents
    function determineParents(obj) {
        let parents = [];

        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (key.includes('Id')) {
                    let serviceName = key.split('Id')[0];
                    serviceName = camelToSnake(serviceName);

                    parents.push(serviceName);
                }
            }
        }

        return parents;
    }

    //string formatting for service names
    function camelToSnake(str) {
        return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    }

    function snakeToCamel(str) {
        return str.replace(/_([a-z])/g, function (g) {
            return g[1].toUpperCase();
        });
    }

    function kebabToSnake(str) {
        return str.replace(/-/g, "_");
    }

    function registerServices(commit) {
        services.forEach(s => {
            const service = Feathers.service(s);
            const serviceName = s.replace('-', '_');
            service.find().then(r => {
                commit('find', {data: r.data, serviceName})
            });
            service.off('created');
            service.on('created', data => {
                commit('create', {data, serviceName})
            });
            service.off('removed');
            service.on('removed', data => {
                commit('remove', {data, serviceName})
            });
            service.off('updated');
            service.on('updated', data => {
                commit('update', {data, serviceName})
            });
            service.off('patched');
            service.on('patched', data => {
                commit('update', {data, serviceName})
            });
        });
    }


    function refresh(commit, rootState) {
        services.forEach(s => {
            const service = Feathers.service(s);
            const serviceName = s.replace('-', '_');
            service.find().then(r => {
                commit('find', {data: r.data, serviceName})
            });
        })
    }

    store.registerModule('data', module, {preserveState: preserve})
};
