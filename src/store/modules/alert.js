export default {
    state: {
        type: null,
        message: '',
        title: ''
    },
    actions: {
        alert({commit}, alert) {
            commit('showAlert', alert)
        }
    },
    mutations: {
        error(state, params) {
            state.type = 'danger';
            state.message = params.message;
            state.title = params.title;
        },
        showAlert(state, params) {
            state.type = params.type;
            state.message = params.message;
            state.title = params.title;
        },

        hideAlert(state) {
            state.type = null;
            state.message = '';
            state.title = '';
        },
    },
    getters: {
        alert: state => {
            return state
        }
    }


}