import * as types from '../mutation-types';
import moment from 'moment';
import Feathers from "../../modules/feathers-client";
import store from "../";
import router from "../../router";
import setupFeathersData from './feathersData';

const state = {
    device: {
        width: 0,
        height: 0
    },
    sidebar: {
        opened: false,
        hidden: true,
    },
    loggedIn: false,
    authenticated: false,
    authenticating: false,
    currentDate: moment().startOf('week'),
};

const getters = {
    loggedIn: state => {
        return state.loggedIn;
    },
    authenticated: state => {
        return state.authenticated;
    },
    authenticating: state => {
        return state.authenticating;
    },

    device: (state) => {
        return state.device;
    },
    isMobile: state => {
        return state.device.width < 768;
    },
    sidebar: (state) => {
        return state.sidebar;
    },

};

const mutations = {

    sidebarHidden(state, val) {
        state.app.sidebar.hidden = val;
    },
    setCurrentDate(state, date) {
        state.currentDate = date.startOf('week');
    },
    authenticate(state, val) {
        state.authenticated = val;
    },
    authenticating(state, val) {
        state.authenticating = val;
    },
    [types.LOGIN](state) {
        state.loggedIn = true;
        state.authenticated = true;
        state.sidebar.hidden = false;
        if (!state.device.isMobile) {
            state.sidebar.opened = true;
        }
    },
    [types.LOGOUT](state) {
        state.loggedIn = false;
        state.authenticated = false;
        state.sidebar.hidden = true;
        state.sidebar.opened = false;

    },
    [types.TOGGLE_DEVICE](state, device) {
        state.device.height = device.height;
        state.device.width = device.width;
    },
    [types.TOGGLE_SIDEBAR](state, config) {
        if (state.device.isMobile && config.hasOwnProperty('opened')) {
            state.sidebar.opened = config.opened;
        } else {
            state.sidebar.opened = true;
        }
        if (config.hasOwnProperty('hidden')) {
            state.sidebar.hidden = config.hidden;
        }
    },
};

const actions = {
    login({commit, dispatch}, formData) {
        Feathers.authenticate({
            user_name: formData.user,
            password: formData.password,
            strategy: 'local'
        }).then(() => {
            Feathers.service('users').find({query: {user_name: formData.user}}).then(res => {
                commit('LOGIN');
                const user = res.data[0];
                commit('user', user);
                setupFeathersData(false, store).then(() => {
                    store.dispatch('populate');
                });
                router.push('/');
                commit('authenticating', false)
            }).catch()
        }).catch(r => {
            commit('authenticating', false);
            // @TODO error
            dispatch('error', r);

        });

    },

    toggleSidebar({commit}, config) {
        if (config instanceof Object) {
            commit(types.TOGGLE_SIDEBAR, config)
        }
    },

    expandMenu({commit}, menuItem) {
        if (menuItem) {
            menuItem.expanded = menuItem.expanded || false;
            commit(types.EXPAND_MENU, menuItem)
        }
    },

    toggleDevice({commit}, device) {
        commit(types.TOGGLE_DEVICE, device)
    },

    error({dispatch, commit}, error) {
        if (error.message === 'jwt expired' || error.message === 'Could not find stored JWT and no authentication strategy was given' || error.message === 'No auth token' || error.message === 'invalid signature') {
            dispatch('criticalError', 'Votre session a expiré.').then(() => {
                dispatch('logout')
            })
        } else if (error.message === 'Socket connection timed out') {
            dispatch('criticalError', 'Erreur de connection au serveur').then(() => {
                dispatch('logout')
            })
        } else {
            if (error.errors && error.errors[0]) {
                let message = 'Erreur';
                switch (error.errors[0].message) {
                    case 'start must be unique':
                        message = 'Cette plage horaire est indisponible.';
                        break;
                }
                commit('error', {message, title: 'Erreur'})
            } else {
                commit('error', {message: error.message, title: 'Erreur'})
            }

        }
    },

    logout({commit}) {

        commit('LOGOUT');
        Feathers.logout();
        router.push('/login');

        try {
            store.unregisterModule('data');
        } catch (e) {
            console.log(e)
        }

    },

    criticalError({commit}, message) {
        commit('LOGOUT');
        commit('error', {message, title: 'Erreur'})
    },

    setCurrentDate({dispatch, commit, state}, date) {
        commit('setCurrentDate', date);
        if (state.user.role !== null) {
            dispatch(state.user.role + '/refresh')
        }
    },
};

export default {
    state,
    mutations,
    getters,
    actions,
};
