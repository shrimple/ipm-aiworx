import router from '../../router'
import component from '@/modules/component'

export default {
    state: [
        {name: 'service', path: '/', displayName: "Service", component: 'Home', icon: 'etudiants-on',},
        {name: 'role', path: '/role', displayName: "Role", component: component('Role'), icon: 'etudiants-on'},
    ],
    mutations: {
        addRoutes: (state, routes) => {
            state.push(...routes);
        }
    },
    actions: {
        addRoutes: ({commit}, routes) => {
            router.addRoutes(routes);
            commit('addRoutes', routes);
        }
    },
    getters: {
        routes: state => {
            return state;
        },
    }
}