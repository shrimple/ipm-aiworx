export default {
    state: {
        id: null,
        user_name: null,
        role: null,
        firstName: null,
        lastName: null,

    },

    mutations: {
        user(state, user) {
            user.id ? state.id = user.id : null;
            user.user_name ? state.user_name = user.user_name : null;
            user.role ? state.role = user.role : null;
            state.firstName = user.firstName;
            state.lastName = user.lastName;

            // generateRoutesFromRole(state.role);
        }
    },

    getters: {
        fullName: state => {
            return state.firstName + " " + state.lastName;
        },
        roleName: state => {
            return state.role && state.role.prettyName;
        },
        user: state => {
            return state;
        },

    }
}