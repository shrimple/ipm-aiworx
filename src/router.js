import Vue from 'vue'
import Router from 'vue-router'
import component from '@/modules/component'

Vue.use(Router);


export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'Connexion',
            path: '/login',
            component: require('@/views/login').default
        },

        ...generateRoutesFromState(),
        {
            path: '*',
            redirect: '/'
        }
    ]
})

function generateRoutesFromState() {
    return [
        {name: 'service', path: '/', component: component('Home'), icon: 'etudiants-on'},
        {name: 'role', path: '/role', component: component('Role'), icon: 'etudiants-on'},
    ]
}
