/**
 * Created by Tristani on 1/15/2018.
 */
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import io from 'socket.io-client';

const authentication = require('@feathersjs/authentication-client');


const socket = io((process.env.NODE_ENV === 'development') ? 'https://admin.operam.io/' : '/');
// const socket = io((process.env.NODE_ENV === 'development') ? 'http://localhost:3030' : '/');


const Feathers = feathers().configure(socketio(socket)).configure(authentication({storage: window.localStorage}));

//console log of every event in the app
(function () {
    // socket.emit = function () {
    // 	console.log('***', 'emit', Array.prototype.slice.call(arguments));
    // 	emit.apply(socket, arguments);
    // };
    if (process.env.NODE_ENV === 'development') {
        const onevent = socket.onevent;
        socket.onevent = function (packet) {
            console.log('***', 'on', Array.prototype.slice.call(packet.data || []));
            onevent.apply(socket, arguments);
        };
    }
}());

export default Feathers;
