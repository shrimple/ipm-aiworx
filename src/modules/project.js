import feathers from './feathers-client';

const axios = require('axios');


const instance = axios.create({
    // baseURL: (process.env.NODE_ENV === 'development') ? 'http://localhost:3030/' : '/',
    baseURL: (process.env.NODE_ENV === 'development') ? 'https://admin.operam.io/' : '/',
    timeout: 5000,
    headers: {"Content-Type": "multipart/form-data"}
});

export const refreshProjects = () => {
    instance.get('refresh-projects').then();
};

export const removeImage = (id) => {
    return new Promise(resolve => {
        feathers.service("image").remove(id);
    });
};

export const uploadImage = (file, pid) => {
    return new Promise(resolve => {
        const formData = new FormData();
        formData.append('image', file);

        instance.post('/upload', formData).then(res => {
            let filePath = res.data.filePath;

            let data = {
                path: filePath + res.data.file.filename,
                projectFormId: pid,
            };

            console.log("upload success filename: " + res.data.file.filename + ".");
            feathers.service("image").create(data);
        }).catch(err => console.warn(err));
    });
};

export const addForm = (form, todos, file) => {
    return new Promise(resolve => {
        feathers.service("project-form").create(form).then(res => {
            addTodos(res.id, todos);

            if (file != null && file != "")
                uploadImage(file, res.id);
            resolve();
        }).catch(err => console.log(err))
    });
};

export const addTodos = (formId, todos) => {
    return new Promise(resolve => {
        const create = [];

        if (todos) {
            todos.forEach(todo => {
                let _todo = todo;
                _todo.projectFormId = formId;

                create.push(_todo);
            });

            if (create.length > 0)
                feathers.service("todo").create(create).then().catch(err => res.json(err));
            resolve()
        } else {
            console.log("todos null!")
        }
    })
};

export const removeTodo = (id) => {
    feathers.service("todo").remove(id).catch(err => console.log("Remove todo '" + id + "' exception: " + err));
};

export const removeForm = (id) => {
    return new Promise(resolve => {
        feathers.service("project-form").remove(id).catch(err => console.log(err));
        resolve();
    });
};

export const patchForm = (fid, form, todos) => {
    return new Promise(resolve => {
        //console.log(form + " | " + fid);

        feathers.service("project-form").patch(fid, form).then(pform => {
                //console.log("patchForm:");

                for (let todo of todos) {
                    if ('id' in todo) {
                        feathers.service("todo").patch(todo.id, todo);
                    } else {
                        console.log(fid);
                        todo.projectFormId = fid;
                        feathers.service("todo").create(todo);
                    }


                }
                resolve();
            }
        );
    })
};

