import feathers from './feathers-client';

const createUsers = async () => {
    const users = []
};

export const remove = (toDel) => {
    feathers.service(toDel.model).remove(toDel.id);
};


export const populate = () => {

    feathers.service('role').create({name: 'etudiant', prettyName: 'Étudiant'}).then(role => {
        feathers.service('users').create({
            firstName: 'Gilles',
            lastName: 'Bread',
            user_name: 'e',
            password: 'e',
            roleId: role.id
        }).then(user => {
            // showAlert('User created.', 'success', 'Job complete!')
        })
    });

};

export const createService = (service) => {
    feathers.service('service').create(service).then((r) => {
        console.log(r)
    });
};

export const permission = (permission) => {

    feathers.service('permission').create(permission).then(() => {
        // showAlert('permission created.', 'success', 'Job complete!')
    });

};

export const createRole = (role) => {
    feathers.service('role').create(role).then((r) => {
        console.log(r)
    });
};


export const addObj = (adding) => {

    switch (adding.model) {
        case 'route':
            route(adding.obj);
            break;

        default:
            feathers.service(adding.model).create(adding.obj).then(() => {
                showAlert(adding.model + ' created.', 'success', 'Job complete!')
            });
    }


};

// export const editObj = (editing) => {
//
// 	feathers.service(editing.model).update(editing.id, editing.obj).then(() => {
// 		showAlert(editing.model + ' created.', 'success', 'Job complete!')
// 	});
//
// };
// export const route = (route) => {
//
//
// 	feathers.service('route').create(route).then(returnedRoute => {
// 		const permission = {name: route.name, routeId: returnedRoute.id, roleId: route.roleId};
// 		feathers.service('permission').create(permission).then(() => {
//
// 			showAlert('Route and permission created.', 'success', 'Job complete!')
//
// 		});
// 	});
//
//
// };
