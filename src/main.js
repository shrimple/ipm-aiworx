import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
import Feathers from './modules/feathers-client.js'
import globalComponents from './components/globals'
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import './assets/styling/app.less'
import {Timeline} from 'vue2vis'

const vueFeathers = require('vue-feathers');

// Vue.use(NProgress);
Vue.use(vueFeathers, Feathers);
// Vue.use(PortalVue);
Vue.component('timeline', Timeline);

globalComponents.components.forEach(comp => {
    Vue.component(comp.name, comp);
});

Vue.use(iView);


router.beforeEach((route, redirect, next) => {
    const userRole = store.state.user.role;
    const userLoggedIn = store.state.app.loggedIn;
    const routePath = route.path;

    const homepage = {
        etudiant: "/home",
    };

    // Si user logged in et tente d'acceder a la page login
    if ((routePath === "/login") && userLoggedIn) {
        next(homepage[userRole]);
    }
    // Si user pas logged in et tente d'acceder a un page != login
    else if (routePath !== "/login" && !userLoggedIn) {
        next("/login");
    }
    else {
        next();
    }


    /*


        if (store.state.app.device.isMobile && store.state.app.sidebar.opened) {
            store.commit(TOGGLE_SIDEBAR, {opened: false})
        }
        if(!store.state.app.loggedIn && route.path !== '/login'){
            next('/login');
        }
        else if(route.path === '/'){
            try {
                next(homepage[store.state.user.role]);
            }
            catch (e) {
                store.dispatch('logout');
                router.push('/login')
            }
        }
        else if(store.state.app.loggedIn && route.path.includes('/login') ){
            try {
                next(homepage[store.state.user.role]);
            }
            catch (e) {
                store.dispatch('logout');
                router.push('/login')
            }
        }
        else{
            next();
        }*/
});

Vue.config.productionTip = false;
new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');
