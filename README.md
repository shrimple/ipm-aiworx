# aiworx-ipm

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Outil mensuelle pour le review de projet. Conçu pour être reliée directement au Interval API. Cet outil a été construit avec Vue.js et Feathers.js.
IPM a été codé en 2018 (en python & django) et a été reconstruit en 2019 (avec vue et feathers). Maintenant pas utilisé, dû au fait que la compagnie qui m'a donnée le contrat s'est fait acheter,
donc Interval, l'outil sur lequel celui que j'ai coder a été fait, est obselète.

Post-mortem:
-Plus de documentation de code
-Instructions sur comment compiler, tester et lancer.
-Plus de fonctions

